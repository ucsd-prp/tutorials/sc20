# SC20 Tutorial

Storage in Kubernetes \
Hands on session

## Using storage

Different Kubernetes clusters will have different storage options available.\
Let’s explore the most basic one: emptyDir. It will allocate the local scratch volume, which will be gone once the pod is destroyed.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
As mentioned before, all the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

###### ev.yaml:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: strg-username
  labels:
    k8s-app: strg-username
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: strg-username
  template:
    metadata: 
      labels:
        k8s-app: strg-username
    spec:
      containers:
      - name: mypod
        image: alpine
        resources:
           limits:
             memory: 500Mi
             cpu: 200m
           requests:
             memory: 100Mi
             cpu: 100m
        command: ["sh", "-c", "apk add dumb-init && dumb-init -- sleep 100000"]
        volumeMounts:
        - name: mydata
          mountPath: /mnt/myscratch
      volumes:
      - name: mydata
        emptyDir: {}
```

Now let’s start the deployment:

`kubectl create -f strg1.yaml`

Now log into the created pod, create

`mkdir /mnt/myscratch/username`

then store some files in it.

Also put some files in some other (unrelated) directories.

Now kill the container: `kill 1` wait for a new one to be created, then log back in.

What happened to files?

You can now delete the deployment.

## Using outer persistent storage

In our cluster we have ceph storage connected, which allows using it for real data persistence.

To get storage, we need to create an abstraction called PersistentVolumeClaim. By doing that we "Claim" some storage space - "Persistent Volume". There will actually be PersistentVolume created, but it's a cluster-wide resource which you can not see.

Create the file (replace username as always):

###### pvc.yaml:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: vol-username
spec:
  storageClassName: rook-ceph-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

We're creating a 1GB volume and formatting it with XFS.

Look at it's status with `kubectl get pvc vol-username` (replace username). The `STATUS` field should be equals to `Bound` - this indicates successful allocation.

Now we can attach it to our pod. Create one with replacing `username`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-username
spec:
  containers:
  - name: mypod
    image: centos:centos7
    command: ["sh", "-c", "sleep infinity"]
    resources:
      limits:
        memory: 100Mi
        cpu: 100m
      requests:
        memory: 100Mi
        cpu: 100m
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: vol-username
```

In volumes section we're attaching the requested persistent volume to the pod (by its name! Don't forget to change `username`), and in volumeMounts we're mounting the attached volume to the container in specified folder.

Now exec into your container and see the mounted rbd (Rados Block Device) volume:

`df`

Clean up the pod and persistentvolumeclaim when you're done. Kubernetes will delete the corresponding persistentvolume and clean up the space taken by your volume.

## Exploring storageClasses

Attaching persistent storage is usually done based on storage class. Different clusters will have different storageClasses, and you have to read the documentation on which one to use. The nautilus storageClasses are described here: https://pacificresearchplatform.org/userdocs/storage/ceph-posix/

You can create other persistentVolumes with the storageclasses which are not marked as restricted in the documentation.

## End
