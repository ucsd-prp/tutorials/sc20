# SC20 Tutorial

## Agenda

* [Intro - Unified Cyber Infrastructure - Slides](slides/1_sc20_k8s_intro.pdf)

* [Kubernetes Architecture - Slides](slides/2_sc20_kubernetes_arch.pdf)

* [Basic Kubernetes - Hands On](2a_Basic hands on.md) 

* [Kubernetes Scheduling - Slides](slides/3_sc20_kubernetes_sched.pdf)

* [Scheduling hands-on](3a_Scheduling hands on.md)

* [Persistent Storage - Slides](slides/4_sc20_kubernetes_rook.pptx)

* [Storage hands-on](4a_Storage hands on.md)

* [How to install Kubernetes - Slides](slides/5_sc20_k8s_install.pdf)


## Feedback

Please fill out the [evaluation form](https://submissions.supercomputing.org/?page=Submit&id=TutorialEvaluation&site=sc20) with the feedback on this Tutorial.

